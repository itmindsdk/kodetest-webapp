import { AzureFunction, Context, HttpRequest } from "@azure/functions";

const httpTrigger: AzureFunction = async function(
  context: Context,
  req: HttpRequest
): Promise<void> {
  context.log("HTTP trigger function processed a request.");
  const response = req.query.response || (req.body && req.body.response);

  if (response) {
    context.res = {
      // status: 200, /* Defaults to 200 */
      body: "Recorded response: " + (req.query.response || req.body.response)
    };
  } else {
    context.res = {
      status: 400,
      body: "Please pass a name on the query string or in the request body"
    };
  }
};

export default httpTrigger;
