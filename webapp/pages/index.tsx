import React, { ReactElement } from "react";
import Layout from "../components/Layout";
import Question1 from "../components/Questions/Question1";
import Question2 from "../components/Questions/Question2";
import Question3 from "../components/Questions/Question3";
import FinalQuestion from "../components/Questions/FinalQuestion";
import FirstQuestion from "../components/Questions/FirstQuestion";
import uuid from "uuid/v4";

type State = {
  questions: ReactElement<{ next: Function }>[],
  currentQuestion: ReactElement<{ next: Function }>,
  startingTime: number,
  roundtripTime: number,
  totalTime: number,
  testUUID: string
}

type Props = {}

class Index extends React.Component<Props, State> {
  readonly _bindNext = this._goToNextQuestion.bind(this);

  readonly state = {
    questions: [<Question1 next={this._bindNext} />, <Question2 next={this._bindNext} />, <Question3 next={this._bindNext} />],
    currentQuestion: <FirstQuestion next={this._bindNext} />,
    startingTime: 0,
    roundtripTime: 0,
    totalTime: 0,
    testUUID: ""
  } //Readonly avoids mutating state without setState

  componentDidMount() {
    // this._insertRandomQuestion();
  }

  render() {
    return (
      <Layout title="Code Test">
        <p>IT Minds Code Test</p>
        <hr />
        {this.state.currentQuestion}
      </Layout>
    )
  }

  _insertRandomQuestion(): void {
    let { questions } = this.state;
    if (questions.length) {
      let question: ReactElement<{ next: Function }> = questions[Math.floor(Math.random() * questions.length)];
      console.log("Inserting", (question.type as any).name); // Name does exist on type
      this.setState({ currentQuestion: question })
    } else {
      // Last question reached
      let lastQuestion: ReactElement<{ next: Function }> = <FinalQuestion next={this._handleFinalSubmit} />;
      let spentTime = (Date.now() - this.state.startingTime);
      console.log(`Spent time: ${spentTime / 1000}s`); // Total spent time in secs
      this.setState({ currentQuestion: lastQuestion, totalTime: spentTime });
    }
  }

  _handleFinalSubmit(): void {
    location.reload();
  }

  _goToNextQuestion(): void {
    let { questions, currentQuestion, startingTime, roundtripTime, testUUID } = this.state;
    // Remove currentQuestion
    if (questions.includes(currentQuestion)) {
      console.log("Removing", currentQuestion.type.name);
      const idToRemove = questions.indexOf(currentQuestion);
      questions.splice(idToRemove, 1);
    }

    if (currentQuestion.type.name === FirstQuestion.name) {
      console.log("Starting Timer");
      this.setState({ startingTime: Date.now(), testUUID: uuid() })
    } else {
      const now = Date.now();
      let rtt = roundtripTime ? roundtripTime : startingTime
      const thisRoundtripTime = now - rtt;
      console.log(`Roundtrip time ${thisRoundtripTime / 1000}s for UUID: ${testUUID}`); // Roundtrip time in secs
      this.setState({ roundtripTime: now });
      // Do something with roundtripTime
    }

    // Insert new random question
    this.setState({ questions: questions }, () => this._insertRandomQuestion())
  }
}

export default Index;
