const withTypescript = require("@zeit/next-typescript");
const withCSS = require("@zeit/next-css");
const withBundleAnalyzer = require("@zeit/next-bundle-analyzer");

module.exports = withBundleAnalyzer(
  withTypescript(
    withCSS({
      cssLoaderOptions: {
        importLoaders: 1,
        localIdentName: "[local]___[hash:base64:5]"
      },
      target: "serverless",
      analyzeServer: ["server", "both"].includes(process.env.BUNDLE_ANALYZE),
      analyzeBrowser: ["browser", "both"].includes(process.env.BUNDLE_ANALYZE),
      bundleAnalyzerConfig: {
        server: {
          analyzerMode: "static",
          reportFilename: "./bundles/server.html"
        },
        browser: {
          analyzerMode: "static",
          reportFilename: "./bundles/client.html"
        }
      },
      webpack(config, options) {
        // Further custom configuration here
        return config;
      },
      exportPathMap: () => {
        return {
          "/": { page: "/" }
        };
      }
    })
  )
);
