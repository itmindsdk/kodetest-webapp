import React from "react";
import { Controlled as CodeMirror } from 'react-codemirror2';

try {
  navigator
  if (process.browser) {
    require('codemirror/mode/javascript/javascript');
    require('codemirror/lib/codemirror.css');
    require('codemirror/theme/material.css');
  }
} catch (error) { }

type State = {
  response: string,
  testPassed: boolean,
  testRan: boolean,
  testErrorMessage: string,
  blurTime: number,
  totalBlurTime: number
}

type Props = { next: Function }

const defaultResponse: string = `console.log("I ♥ IT Minds");`;

class Question1 extends React.Component<Props, State> {
  state = {
    response: defaultResponse,
    testPassed: false,
    testRan: false,
    testErrorMessage: "",
    blurTime: 0,
    totalBlurTime: 0
  }

  componentDidMount() {
    window.addEventListener("blur", this._blurEventListener);
    window.addEventListener("focus", this._focusEventListener);
  }

  _blurEventListener = (): void => {
    console.log("Starting blur timer");
    this.setState({ blurTime: Date.now() });
  }

  _focusEventListener = (): void => {
    const { blurTime, totalBlurTime } = this.state;
    const now = Date.now();
    const bt = blurTime ? blurTime : 0;
    const tbt = totalBlurTime ? totalBlurTime : 0;

    const thisBlurTime = now - bt;
    const thisTotalBlurTime = tbt + thisBlurTime;

    console.log("Stopping blur timer after " + thisBlurTime / 1000 + "s");
    this.setState({ blurTime: now, totalBlurTime: thisTotalBlurTime });
  }

  componentWillUnmount() {
    window.removeEventListener("blur", this._blurEventListener);
    window.removeEventListener("focus", this._focusEventListener);
  }

  render() {
    const { testPassed, testErrorMessage, testRan } = this.state;
    return (
      <div>
        <h2>Question 1: Bro, do you even code?</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          <br />
          <br />
          Press the Test button below or 'Ctrl+Enter' to evaluate your code.
        </p>
        <form name="response" onSubmit={this._handleSubmit}>
          <CodeMirror
            value={this.state.response}
            options={{
              mode: 'javascript',
              theme: 'material',
              lineNumbers: true
            }}
            onBeforeChange={(editor: CodeMirror.Editor, data: CodeMirror.EditorChange, value: string) => {
              // data can be used to log events such as: input, delete, paste, copy
              this.setState({ response: value });
            }}
            onKeyPress={(editor: CodeMirror.Editor, event: any) => {
              if (event.code === "Enter" && event.ctrlKey === true) {
                this._handleTest();
              }
            }}
          />
          <br />
          <button type="button" onClick={this._handleTest}>Test</button>
          <input type="submit" value="Submit" />
          {testRan && <p>{testPassed ? "Code runs without errors" : "Error: " + testErrorMessage}</p>}
        </form>
      </div>
    )
  }

  _handleResponseChange(event: React.ChangeEvent<HTMLTextAreaElement>): void {
    this.setState({ response: event.target.value })
  }

  _handleTest = (): void => {
    const { response, testRan, testPassed } = this.state;
    console.log("Testing response: " + response);
    try {
      eval(response);
      !testPassed && this.setState({ testPassed: true })
    } catch (e) {
      this.setState({ testPassed: false, testErrorMessage: e.message })
    }
    !testRan && this.setState({ testRan: true })
  }

  _handleSubmit = (event: any): void => {
    const response = this.state.response;
    const azureEndpoint = `http://localhost:7071/api/Question1?response=${response}`;

    event.preventDefault(); // Avoid redirecting to new page on submit
    window.history.replaceState("", "", "/"); // Removes query params name and mail from URL

    console.log("Total blur time", this.state.totalBlurTime / 1000 + "s")

    fetch(azureEndpoint)
      .then(res => {
        if (res.ok) {
          return res.text();
        } else {
          throw new Error("Something went wrong")
        }
      })
      .then(text => console.log(text))
      .catch(e => console.log(e))
      .finally(() => this.props.next());
  }
}

export default Question1;
