import React from "react";

type State = {
  response: string
}

type Props = { next: Function }

class FinalQuestion extends React.Component<Props, State> {
  state = {
    response: "Any final words?",
  }

  render() {
    return (<div>
      <h2>Thanks for submitting your answer!</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>
      <form name="response">
        <textarea name="message" rows={10} cols={30} value={this.state.response} onChange={e => this._handleResponseChange(e)} />
      </form>
      <br />
      <button type="submit" onClick={this._handleSubmit} >Submit</button>
    </div>)
  }

  _handleResponseChange(event: React.ChangeEvent<HTMLTextAreaElement>): void {
    this.setState({ response: event.target.value })
  }

  _handleSubmit = (): void => {
    alert("Recorded response: " + this.state.response);
    this.props.next();
  }
}

export default FinalQuestion;
