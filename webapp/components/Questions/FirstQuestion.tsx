import React from "react";

type State = {
  name: string,
  mail: string
}

type Props = { next: Function }

class FirstQuestion extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      name: "",
      mail: ""
    }
    this._handleSubmit = this._handleSubmit.bind(this);
  }

  render() {
    return (
      <div>
        <h2>Hello IT Minds applicant!</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
        <form name="response" onSubmit={this._handleSubmit}>
          Name <input required type="text" name="name" value={this.state.name} onChange={e => this._handleNameChange(e)} />
          <br />
          E-mail <input required type="email" name="mail" value={this.state.mail} onChange={e => this._handleMailChange(e)} />
          <br /><br />
          <input type="submit" value="Start"></input>
        </form>
      </div>
    )
  }

  _handleNameChange(event: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({ name: event.target.value })
  }
  _handleMailChange(event: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({ mail: event.target.value })
  }

  _handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    event.preventDefault();
    window.history.replaceState("", "", "/"); // Removes query params name and mail from URL
    const { name, mail } = this.state;
    alert("Recorded name: " + name + " and e-mail: " + mail);
    this.props.next();
  }
}

export default FirstQuestion;
