import React from "react";

type State = {
  response: string
}

type Props = { next: Function }

class Question2 extends React.Component<Props, State> {
  state = {
    response: "",
  }

  render() {
    return (
      <div>
        <h2>Question 2: Radio Buttons</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
        <form name="response" onChange={this._handleChange}>
          <input type="radio" name="gender" value="male" />Male<br />
          <input type="radio" name="gender" value="female" />Female<br />
          <input type="radio" name="gender" value="other" />Other
        </form>
        <br />
        <button type="submit" onClick={this._handleSubmit} >Submit</button>
      </div>
    )
  }

  _handleChange = (event: React.ChangeEvent<HTMLFormElement>): void => {
    const value = event.target.value;
    this.setState({ response: value });
  }

  _handleSubmit = (): void => {
    const response = this.state.response;
    if (response === "") {
      alert("No answer was given");
      return;
    }
    alert("Recorded response: " + response);
    this.props.next();
  }
}

export default Question2;
