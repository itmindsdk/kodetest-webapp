import React from "react";

type State = {
  response: Array<string>
}

type Props = { next: Function }


class Question3 extends React.Component<Props, State> {
  state = {
    response: [],
  }

  render() {
    return (
      <div>
        <h2>Question 3: Checkboxes</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
        <form name="response" onChange={this._handleChange}>
          <input type="checkbox" name="language" value="js" />
          JavaScript
          <br />
          <input type="checkbox" name="language" value="ts" />
          TypeScript
          <br />
          <input type="checkbox" name="language" value="python" />
          Python
        </form>
        <br />
        <button type="submit" onClick={this._handleSubmit} >Submit</button>
      </div>
    )
  }

  _handleChange = (event: any): void => {
    const response: string[] = [...this.state.response];
    const value: string = event.target.value;

    if (response.includes(value)) {
      const id = response.indexOf(value);
      response.splice(id, 1);
    } else {
      response.push(value);
    }

    this.setState({ response: response });
  }

  _handleSubmit = (): void => {
    const response = this.state.response;
    if (response.length === 0) {
      alert("No answer was given");
      return;
    }
    alert("Recorded response: " + response);
    this.props.next();
  }
}

export default Question3;
